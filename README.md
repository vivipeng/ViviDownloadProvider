#ViviDownloadProvider

# 使用方法

## 集成

1. 用android studio导入downloadProvider
2. 修改downloadProvider的gradle文件,如下:

        // DownloadProvider的配置
        def targetAppId = 'com.vivi.test.downloadprovider'
        buildConfigField 'String', 'DOWNLOAD_PROVIDER_TARGET_APPLICATION_ID', "\"${targetAppId}\""
        manifestPlaceholders = [DOWNLOAD_PROVIDER_TARGET_APPLICATION_ID: "${targetAppId}"]

targetAppId 是开发者的app的appId, 用自己的appId替换这个值就可以了


## 开发(可参考app中代码)

### 下载

        void loadFile(Context context) {
            Uri srcUri = Uri.parse(URI_FILE);
            DownloadManager.Request request = new DownloadManager.Request(srcUri);
            String fileName = "123456.apk";
    
            try {
                File file = new File(FileManager.getApksDownloadPath());
                Uri desUri = Uri.withAppendedPath(Uri.fromFile(file), fileName);
                request.setDestinationUri(desUri);
                DownloadManager downloadManager = new DownloadManager(context.getContentResolver(), context.getPackageName());
                downloadManager.enqueue(request);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
    
        }


### 暂停

待定...


### 取消/删除 下载任务

待定...




