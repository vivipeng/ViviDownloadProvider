
## DownloadProvider 安全控制

    添加新的下载任务之前会检查请求者是否有权限。

    每个下载任务信息有个请求者的packageName，发送广播的时候会根据这个packageName指定发送。
    所以User不用担心多个app的情况下，app1发送的广播会被app2收到。

    provider 的export属性设置成false， 外部也就没机会使用下载服务了。




## 使用方法

1. 修改UserConfig里的permission和authority

2. AndroidManifest.xml中的use-permission也需要修改

