package cn.vivi.test.downloadprovider;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.vivi.test.downloadprovider.R;

import java.util.List;

import cn.vivi35.downloadprovider.app.DownloadManagerFile;
import cn.vivi35.downloadprovider.app.DownloadManagerHelper;


public class FileListFragment extends Fragment {
    private ListView mFileListView;
    private FileAdapter mFileAdapter;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_file_list, null);

        mFileAdapter = new FileAdapter(getActivity());
        mFileListView = (ListView) view.findViewById(R.id.lvFiles);
        mFileListView.setAdapter(mFileAdapter);

        return view;
    }

    public void updateFile(DownloadManagerFile file) {
        mFileAdapter.updateFile(file);

    }


    private class FileAdapter extends BaseAdapter {
        private static final String TAG = "FileAdapter";
        private List<DownloadManagerFile> mData = null;
        private LayoutInflater mInflater;

        public FileAdapter(Context context) {
            mData = DownloadManagerHelper.findAllFilesOfStatus(context);
            mInflater = LayoutInflater.from(context);
        }

        @Override
        public int getCount() {
            return mData.size();
        }

        @Override
        public Object getItem(int arg0) {
            return null;
        }

        @Override
        public long getItemId(int id) {
            return 0;
        }

        @Override
        public View getView(int position, View view, ViewGroup arg2) {
            Log.i(TAG, "getView position == " + position);
            ViewHolder holder = null;
            if (view == null) {
                view = this.mInflater.inflate(R.layout.listitem_download_file, null);
                holder = new ViewHolder();
                holder.tvFileName = (TextView) view.findViewById(R.id.tvFileName);
                holder.tvStatuCode = (TextView) view.findViewById(R.id.tvStatuCode);
                holder.tvReasonCode = (TextView) view.findViewById(R.id.tvReasonCode);
                holder.tvCurrSize = (TextView) view.findViewById(R.id.tvCurrSize);
                ;
                holder.tvTotalSize = (TextView) view.findViewById(R.id.tvTotalSize);

                view.setTag(holder);

            } else {
                holder = (ViewHolder) view.getTag();
            }

            DownloadManagerFile file = mData.get(position);
            holder.tvStatuCode.setText("状态码status: "+file.getStatus());
            holder.tvReasonCode.setText("报错原因reason: "+file.getReason());
            holder.tvFileName.setText("文件名: "+file.getFileName());
            holder.tvTotalSize.setText("总大小: " + file.getTotalSizeBytes());
            holder.tvCurrSize.setText("已下载: " + file.getBytesDownloadedSoFar());

            return view;
        }


        private DownloadManagerFile searchFile(DownloadManagerFile upFile) {
            if (mData != null && mData.size() > 0) {
                for (DownloadManagerFile file : mData) {
                    if (file.getId() == upFile.getId()) {
                        return file;
                    }
                }
            }
            return null;
        }


        /**
         * @param upFile
         */
        public void updateFile(DownloadManagerFile upFile) {
            DownloadManagerFile file = searchFile(upFile);
            if (file == null) {
                mData.add(0, upFile);

            } else {
                file.updateFrom(upFile);
            }
            notifyDataSetChanged();
        }


    }

    private final class ViewHolder {
        public TextView tvFileName;
        public TextView tvReasonCode;
        public TextView tvStatuCode;
        public TextView tvTotalSize;
        public TextView tvCurrSize;
    }


}
