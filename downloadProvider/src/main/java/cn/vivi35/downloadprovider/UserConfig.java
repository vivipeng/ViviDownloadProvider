package cn.vivi35.downloadprovider;

/**
 * Created by Vivi__Peng on 2015/11/20.
 */
public class UserConfig {

    public static final String PREFIX = "cn.vivi35.downloadprovider";

    /**
     * DownloadProvider authority
     */
    public static final String AUTHORITY = PREFIX + ".downloads." + BuildConfig.DOWNLOAD_PROVIDER_TARGET_APPLICATION_ID;


    /**
     * The permission to access the download manager
     *
     * @hide
     */
    public static final String PERMISSION_ACCESS = PREFIX + ".permission.ACCESS_DOWNLOAD_MANAGER";

    /**
     * The permission to access the download manager's advanced functions
     *
     * @hide
     */
    public static final String PERMISSION_ACCESS_ADVANCED = PREFIX + ".permission.ACCESS_DOWNLOAD_MANAGER_ADVANCED";

    /**
     * The permission to access the all the downloads in the manager.
     */
    public static final String PERMISSION_ACCESS_ALL = PREFIX + ".permission.ACCESS_ALL_DOWNLOADS";

    /**
     * The permission to send broadcasts on download completion
     *
     * @hide
     */
    public static final String PERMISSION_SEND_INTENTS = PREFIX + ".permission.SEND_DOWNLOAD_COMPLETED_INTENTS";

    /**
     * The permission to download files without any system notification being
     * shown.
     */
    public static final String PERMISSION_NO_NOTIFICATION = PREFIX + ".permission.DOWNLOAD_WITHOUT_NOTIFICATION";

}
