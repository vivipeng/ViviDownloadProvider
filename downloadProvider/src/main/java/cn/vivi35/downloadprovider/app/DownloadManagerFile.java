package cn.vivi35.downloadprovider.app;



import java.io.File;
import java.io.Serializable;

import android.database.Cursor;
import android.text.TextUtils;


/**
 * DownloadManager下载文件的信息
 * 
 * @author Vivi Peng
 * 
 * @version 1.0.0
 * 
 * @date 2014-11-26
 */
public final class DownloadManagerFile implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 4468474105450098709L;
	
	private Long id = -1L;
	private Long lastModifiedTimestamp = -1L;
	private String fileName;
	private String localUri;
	private int status = DownloadManager.STATUS_PENDING;
	private String title;
	private long totalSizeBytes = 0L;
	private String uri;
	/**
	 * 保存在本地的绝对路径
	 */
	private String absFilename; 
	private long bytesDownloadedSoFar = 0L;
	/**
	 * 原因（STATUS_PAUSE和STATUS_FAILED）
	 */
	private int reason = DownloadManager.PAUSED_UNKNOWN;


	
	public int getReason() {
		return reason;
	}


	
	public void setReason(int reason) {
		this.reason = reason;
	}


	public String getAbsFilename() {
		return absFilename;
	}


	public void setAbsFilename(String absFilename) {
		this.absFilename = absFilename;
	}


	public void updateFrom(DownloadManagerFile srcFile) {
		this.setAbsFilename(srcFile.getAbsFilename());
		this.setBytesDownloadedSoFar(srcFile.getBytesDownloadedSoFar());
		this.setFileName(srcFile.getFileName());
		this.setId(srcFile.getId());
		this.setLastModifiedTimestamp(srcFile.getLastModifiedTimestamp());
		this.setLocalUri(srcFile.getLocalUri());
		this.setReason(srcFile.getReason());
		this.setTitle(srcFile.getTitle());
		this.setTotalSizeBytes(srcFile.getTotalSizeBytes());
		this.setUri(srcFile.getUri());
		this.setStatus(srcFile.getStatus());

	}


	public static DownloadManagerFile newInstance(Cursor c) {
		int status = c.getInt(c.getColumnIndex(DownloadManager.COLUMN_STATUS));
		if (status != DownloadManager.STATUS_SUCCESSFUL) {
//			Log.w("Download", "没有下载成功.");
//			return null;
		}
		DownloadManagerFile file = new DownloadManagerFile();
		long id = c.getLong(c.getColumnIndex(DownloadManager.COLUMN_ID));
		long lastModifiedTimestamp = c.getLong(c
				.getColumnIndex(DownloadManager.COLUMN_LAST_MODIFIED_TIMESTAMP));
		long totalSizeBytes = c.getLong(c.getColumnIndex(DownloadManager.COLUMN_TOTAL_SIZE_BYTES));
		long bytesDownloadedSoFar = c.getLong(c
				.getColumnIndex(DownloadManager.COLUMN_BYTES_DOWNLOADED_SO_FAR));

		/*if (Build.VERSION.SDK_INT >= 11) {
			String absFilename = c.getString(c.getColumnIndex(DownloadManager.COLUMN_LOCAL_FILENAME));
			File tmpFile = new File(absFilename);
			file.setFileName(tmpFile.getName());
			file.setAbsFilename(absFilename);
		} else {*/
			String localUri = c.getString(c.getColumnIndex(DownloadManager.COLUMN_LOCAL_URI));
			file.setLocalUri(localUri);// e.g
										// file:///mnt/sdcard/Download/SingleSpirit/download/apks/com.androidn.pool_bottom.apk
			if (!TextUtils.isEmpty(localUri) && localUri.length() > "file://".length()) {
				String absFilename = localUri.substring("file://".length());
				File tmpFile = new File(absFilename);
				file.setFileName(tmpFile.getName());
				file.setAbsFilename(absFilename);
			}
//		}
		String title = c.getString(c.getColumnIndex(DownloadManager.COLUMN_TITLE));
		String uri = c.getString(c.getColumnIndex(DownloadManager.COLUMN_URI));
		int reasonCode = c.getInt(c.getColumnIndex(DownloadManager.COLUMN_REASON));

		file.setId(id);
		file.setLastModifiedTimestamp(lastModifiedTimestamp);
		file.setTotalSizeBytes(totalSizeBytes);
		file.setBytesDownloadedSoFar(bytesDownloadedSoFar);
		file.setTitle(title);
		file.setUri(uri);
		file.setStatus(status);
		file.setReason(reasonCode);

		return file;
	}


	public Long getId() {
		return id;
	}


	public void setId(Long id) {
		this.id = id;
	}


	public Long getLastModifiedTimestamp() {
		return lastModifiedTimestamp;
	}


	public void setLastModifiedTimestamp(Long lastModifiedTimestamp) {
		this.lastModifiedTimestamp = lastModifiedTimestamp;
	}


	public String getFileName() {
		return fileName;
	}


	public void setFileName(String fileName) {
		this.fileName = fileName;
	}


	public String getLocalUri() {
		return localUri;
	}


	public void setLocalUri(String localUri) {
		this.localUri = localUri;
	}


	public int getStatus() {
		return status;
	}


	public void setStatus(int status) {
		this.status = status;
	}


	public String getTitle() {
		return title;
	}


	public void setTitle(String title) {
		this.title = title;
	}


	public long getTotalSizeBytes() {
		return totalSizeBytes;
	}


	public void setTotalSizeBytes(long totalSizeBytes) {
		this.totalSizeBytes = totalSizeBytes;
	}


	public String getUri() {
		return uri;
	}


	public void setUri(String uri) {
		this.uri = uri;
	}


	public long getBytesDownloadedSoFar() {
		return bytesDownloadedSoFar;
	}


	public void setBytesDownloadedSoFar(long bytesDownloadedSoFar) {
		this.bytesDownloadedSoFar = bytesDownloadedSoFar;
	}



	@Override
	public String toString() {
		return "DownloadManagerFile [id=" + id + ", lastModifiedTimestamp=" + lastModifiedTimestamp
				+ ", fileName=" + fileName + ", localUri=" + localUri + ", status=" + status + ", title="
				+ title + ", totalSizeBytes=" + totalSizeBytes + ", uri=" + uri + ", absFilename="
				+ absFilename + ", bytesDownloadedSoFar=" + bytesDownloadedSoFar + ", reason=" + reason + "]";
	}

	
}
